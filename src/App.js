import React, { Component } from 'react';

import './App.css';
import Numbers from "./Components/Numders/Numbers";



class App extends Component {
    state = {
        numbers :[]
    };

    channgeNumders = ()=>{

        let newNumbers = [];
        while(newNumbers.length < 5) {
            const randNumber = Math.floor(Math.random() * (36-5) + 5);
            if(!newNumbers.includes(randNumber)) {
                newNumbers.push(randNumber)
            }
        }

        let copyState = [...this.state.numbers];
        copyState = newNumbers;
        copyState.sort((a,b) => a - b);

        this.setState({
            numbers: copyState
        });
        console.log(this.state.numbers);
    };

  render() {
    return (
      <div className="App">
          {this.state.numbers.map((number, index) => {
              return (
                  <Numbers number={number} key={index} />
              )
          })}
          <div>
              <button onClick={this.channgeNumders}>cNew numbers</button>
          </div>
      </div>
    );
  }
}

export default App;
