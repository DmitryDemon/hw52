import React from 'react';

import './Numbers.css';

const Number = (props) => {
    return (
        <div className='numLotto'>
            <span className='num'>{props.number}</span>
        </div>
    )
};

export default Number;